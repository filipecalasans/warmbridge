## WARM Bridge - Description

This software is a brige TCP <-> Serial to communicate with the WARM Framework developed at LARC - USP. This software is a multi-thread TCP server that accpets WarmSerialMsg serialized
in json, enabling remote communication with the WSN base station.

## WarmSerialMsg Description

We serialize the class WarmSerialMsg on json format before sending data over the TCP. 
Therefore, it is possible to telnet the server with json commands to ensure the communication 
channel is working properly.

Json packets must be terminated with the ASC character end of line '\n'.

###Json Packet Example

    Data type descrition for each field:
        type: String "warmMesssage" that indicates the packet content type.
        nodeId = Integer
        flowId = Integer
        len = short: size in bytes of data array, an array of bytes (8-bit).
        data: hex string representation of data payload (array of short = 8-bit).

Example:

    {"nodeId":16,"data":"00000010002000300040","len":10,"type":"warmMessage","flowId":10}

## MEssage Type: "Recommendation"

The bridge will accept the packet type "recomendation". The bridge will translate the packet to 
WarmSerialMsg format and send through the serial port to Warm FrameWork Controller Node in order to 
reconfigure the temperature mesurement task period.

## Compilation and Deploy Recommendations

Iy you will deploy this software to physical nodes that communicates thought the Serial COM port emulated over the USB (/dev/ttyUSB or /dev/ttyS0),
you will need to include the toscomm.so library directory to the list of Native Libraries used by this project. This project was compiled using
eclipse IDE using the following steps:

 > Go to:
    Project Properties > Java Build Path > Source > Native library location 

 > Add the TOSROOT/tos/tools/tinyos/java/serial

 > Compile the TOSROOT/tos/tools directory using the following steps taken from the tinyOS documentation:
        >  ./Bootstrap
        > ./configure
        > make
        > sudo make install
    
Make sure to have installed the libraries: gcc-multilib , g++-multilib,  bx32gcc-4.8-dev, libc6-dev if you are using a 64-bit OS
and want to compile the tools to 32-bits.

Under the TOSROOT/tos/tools/tinyos/java/serial you must rename the library file libtoscomm-32.so or libtoscomm-64.so depending 
if you are compiling this source to a 32 or 64 bit JVM. At the time this README file was written, we compiled the source to 
JDK1.8 32-bits and made a symbolic link between libtoscomm-32.so and libtoscomm.so 

Deploy the firmeware to the nodes following the intructions available on the official web site:
    http://tinyos.stanford.edu/tinyos-wiki/index.php/Quickstart:_TelosB

NOTE: There is a compatibility issue between PySerial 3.0 and PySerial 2.X used in the tinyOS official programmer. To solve that problem change
the folowwing line in the source code with error: 
        
    self._port.setBaudrate(baud) by self._port.baudrate = baud
             

