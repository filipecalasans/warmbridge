package src;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

public class SingleThreadTcpServer  {

	@Option(name="-port",usage="Set the port that will connect with dashboard to listen notifications.")
	private int serverPort = 60060;

	@Option(name="-framework",usage="Set the address in which the framework will receive the instructions. \n Possible formats: \n\t serial@/usr/dev/tty \n\t network@<IP>:<PORT>")
	private String serialFramework = null;

	@Option(name="-sink",usage="Set the address in which the sink will deliver the data. \n Possible formats: \n\t serial@/usr/dev/tty \n\t network@<IP>:<PORT>")
	private String serialSink = null;
	
	@Option(name="-dashboard",usage="Set the address in which the Dashboard will be available.")
	private String dashboardAddr = "localhost";
	
	@Option(name="-webport",usage="Set the port that will connect with dashboard to send requests.")
	private int webPort = 1337;
	
	@Option(name="-gwid",usage="Set the gateway ID.")
	private int gatewayId = 1;

	@Option(name="-debug",usage="Turn debug on/off.")
	private boolean DEBUG_MODE = false;
	
	@Option(name="-log",usage="Set the log filename format.")
	private String logfile = "info-%s.log";
	
	
	public static Handler fh;


	/**
	 * Constructor
	 * 
	 * @throws Exception
	 */
	public SingleThreadTcpServer() throws Exception  {
		
	}

	/**
	 * Wait for one new connection at a time
	 * 
	 * @throws Exception
	 */
	public void waitConnections() throws Exception {		
			
			// start the bridge
			Runnable bridgeFramework = new FrameworkSerialThread(serialFramework, gatewayId, "http://" + dashboardAddr + ":" + webPort, dashboardAddr, serverPort, DEBUG_MODE);
			// launch a thread with the connection
			new Thread(bridgeFramework).start();

			
			// start the bridge
			Runnable bridgeSink = new SinkSerialThread(serialSink, "http://" + dashboardAddr + ":" + webPort, DEBUG_MODE);
			// launch a thread with the connection
			new Thread(bridgeSink).start();
	}



	/*** 
	 * doMain - Method to check the command line arguments
	 *  
	 * @param args
	 * @return
	 * @throws IOException
	 */
	public boolean doMain(String[] args) throws IOException {
		
		CmdLineParser parser = new CmdLineParser(this);


		try {
			// parse the arguments.
			parser.parseArgument(args);

		} catch( CmdLineException e ) {
			// if there's a problem in the command line,
			// you'll get this exception. this will report
			// an error message.
			System.err.println(e.getMessage());
			System.err.println("java bridge [options...] arguments...");
			// print the list of available options
			parser.printUsage(System.err);
			System.err.println();

			return false;
		}

		boolean foundErrors = false;
		if(serialFramework == null){
			System.err.println("Set the framework connection using the parameter -framework");
			foundErrors = true;
		}

		if(foundErrors){
			parser.printUsage(System.err);
			return false;
		}

		System.out.println("Connecting to dashboard at port " + serverPort);
		System.out.println("Connecting to the framework in: " + serialFramework);
		System.out.println("Setting gateway ID to: " + gatewayId);

		// create log file for this execution
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SingleThreadTcpServer.fh = new FileHandler(String.format(logfile, sdf.format(new Date())));
		SingleThreadTcpServer.fh.setFormatter(new SimpleFormatter());
		Logger.getLogger("").addHandler(fh);
		
		return true;
	}
	


	
	public static void main(String args[]) throws Exception {
		
		SingleThreadTcpServer server = new SingleThreadTcpServer();

		// check arguments
		if(server.doMain(args)){

			// start the connection
			server.waitConnections();
		}
	}
}
