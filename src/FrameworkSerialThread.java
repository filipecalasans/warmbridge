package src;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import javapackets.InstantaneousTaskSchedulingRequest;
import javapackets.NodeAssociateConfirmation;
import javapackets.TaskDescriptionRequest;
import javapackets.WarmSerialMsg;
import net.tinyos.message.Message;
import net.tinyos.message.MessageListener;
import net.tinyos.message.MoteIF;
import net.tinyos.packet.BuildSource;
import net.tinyos.packet.PhoenixSource;
import net.tinyos.util.PrintStreamMessenger;
import src.lib.ConfirmedCancelation;
import src.lib.TimestampedRecommendation;
import src.lib.Utils;

public class FrameworkSerialThread implements Runnable, MessageListener {

	/* variables to maintain the TCP connection */
	private static final String MSG_HEARTBEAT = "PING\n\r";
	private static final int heartbeatInterval = 12000; // 10s
	private static final int heartbeatTimeoutInterval = 15000; // 15s

	private static final int _PERIODIC_JOIN_TIMEOUT = 30000;
	private static final int _FIRST_JOIN_TIMEOUT = 30000;
	private static final int _CANCEL_THREAD_TIMEOUT = 3000;
	private static final int _RECOMMENDATION_THREAD_TIMEOUT = 3000;
	private final byte NODE_CHARACTERISTICS_DESCRIPTION_PID = 0;
	private final byte NODE_ASSOCIATE_CONFIRMATION_PID = 0x1;
	private final byte TASK_DESCRIPTION_RESPONSE_PID = 0x2;
	private final byte PERIODIC_TASK_SCHEDULING_CONFIRMATION_PID = 0x3;
	private final byte INSTANTANEOUS_TASK_SCHEDULING_REQUEST_PID = 0x4;
	private final byte INSTANTANEOUS_TASK_SCHEDULING_CONFIRMATION_PID = 0x4;
	private final byte SCHEDULED_TASK_CANCELLATION_RESPONSE_PID = 0x6;


	private final byte NODE_STATISTICS_RESPONSE_PID = 0x8;



	private final String dashboardUrl;
	private final int gatewayId;
	private final boolean DEBUG_MODE;
	private final String dashboardAddr;
	private final int serverPort;

	/* default period between two measurements */
	private static final int DEFAULT_PERIOD = 30000;

	/* connection variables */
	private Socket clientSocket;
	private PhoenixSource phoenixFramework;
	private MoteIF moteIfFramework;


	/* registered nodes */
	protected Map<Integer, Timestamp> registeredNodes;
	protected List<Integer> newNodes;

	/* recommendations to transmit */
	private Map<Integer, TimestampedRecommendation> newRecommendations;

	/* recommendations to transmit */
	private Map<Integer, TimestampedRecommendation> queueRecommendations;

	/* queue to cancel */
	private Map<Integer, ConfirmedCancelation> queueToCancel;

	public MoteIF getMoteIfFramework() {
		return moteIfFramework;
	}

	public List<Integer> getNewNodes() {
		return newNodes;
	}

	public void setNewNodes(List<Integer> newNodes) {
		this.newNodes = newNodes;
	}

	public Map<Integer, ConfirmedCancelation> getQueueToCancel() {
		return queueToCancel;
	}

	public Map<Integer, TimestampedRecommendation> getQueueRecommendations() {
		return queueRecommendations;
	}

	public void setQueueRecommendations(Map<Integer, TimestampedRecommendation> queueRecommendations) {
		this.queueRecommendations = queueRecommendations;
	}

	FrameworkSerialThread(String serialFramework, final int gatewayId, String dashboardUrl, String dashboardAddr, int serverPort, boolean debugMode) {
		this.gatewayId = gatewayId;
		this.dashboardUrl = dashboardUrl;
		this.DEBUG_MODE = debugMode;

		this.dashboardAddr = dashboardAddr;
		this.serverPort = serverPort;

		this.newRecommendations = new HashMap<Integer, TimestampedRecommendation>();
		this.queueRecommendations = new HashMap<Integer, TimestampedRecommendation>();

		// connection with the framework
		phoenixFramework = BuildSource.makePhoenix(serialFramework, PrintStreamMessenger.err);
		moteIfFramework = new MoteIF(phoenixFramework);
		moteIfFramework.registerListener(new WarmSerialMsg(), this);		

		// list of nodes
		registeredNodes = new HashMap<Integer, Timestamp>();
		setNewNodes(new ArrayList<Integer>());

		// queue to cancel tasks
		queueToCancel = new HashMap<Integer, ConfirmedCancelation>();

		// nodes have 30s to JOIN

		/* temporally manually starting nodes ... */
		//getNewNodes().add(2);
		//getNewNodes().add(4);
		//getNewNodes().add(5);
		//getNewNodes().add(6);
		//getNewNodes().add(7);
		/*/// */

		TimerTask taskOne = new StartupThread(this, gatewayId, DEFAULT_PERIOD, DEBUG_MODE);
		new java.util.Timer().schedule( taskOne, _FIRST_JOIN_TIMEOUT, _PERIODIC_JOIN_TIMEOUT ); // every 30s

		TimerTask taskCancel = new CancelTaskThread(this, DEBUG_MODE);
		new java.util.Timer().schedule( taskCancel, _FIRST_JOIN_TIMEOUT, _CANCEL_THREAD_TIMEOUT ); // every 0.05s

		TimerTask taskRecommend = new RecommendationThread(this, gatewayId, DEBUG_MODE);
		new java.util.Timer().schedule( taskRecommend, _FIRST_JOIN_TIMEOUT, _RECOMMENDATION_THREAD_TIMEOUT ); // every 0.05s
	}


	public void connect() throws UnknownHostException, IOException{

		// start to listen to a port
		clientSocket = new Socket(dashboardAddr, serverPort);
		clientSocket.setKeepAlive(true);
		clientSocket.setSoTimeout(400);
		clientSocket.setTcpNoDelay(true);
	}

	@Override
	public void run() {

		JSONParser parser = new JSONParser();
		long lastRead = System.currentTimeMillis();
		BufferedReader input;

		clientSocket = new Socket();

		// register "heartbeat" thread -- used to maintain the TCP connection
		registerHeartbeat();

		while (true) {
			try {

				while(clientSocket == null || !clientSocket.isConnected()){ 
					Utils.DEBUG(DEBUG_MODE, "Trying to connect with Dashboard...");
					this.connect();
					lastRead = System.currentTimeMillis();

					// check if it connected
					if(clientSocket != null && clientSocket.isConnected()){
						Utils.DEBUG(DEBUG_MODE, "Connected to server: " + clientSocket.getInetAddress().toString());
					}else{
						Thread.sleep(5000);
					}
				}

				input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

				String command = input.readLine();

				lastRead = System.currentTimeMillis();

				// whenever receiving the heart beat reply, just ignore it.
				if (command != null && MSG_HEARTBEAT.trim().equals(command)){
					// do nothing
					Utils.DEBUG(DEBUG_MODE, "Received heartbeat");
				}else{

					Utils.DEBUG(DEBUG_MODE, "Received: '" + command + "'");
					// if the server has closed connection, it may send a final message
					if(command != null && clientSocket.isConnected() && command.trim().length() > 0){

						if(isJSONValid(command) && isJSONStringObject(command)){ // first, check if it is a JSON object
							JSONObject json = (JSONObject) parser.parse(command);
							Utils.DEBUG(DEBUG_MODE, "... considered as a message from Dashboard.");
							// parse the JSON string and update the node
							decodeJsonPacket(json);
						}else{
							// it was not JSON
							Utils.DEBUG(DEBUG_MODE, "... not a valid JSON. Ignoring it.");
						}
					}
				}
			}catch(ParseException e){
				Utils.DEBUG(DEBUG_MODE, "Got an invalid JSON, ignoring it.");
				e.printStackTrace();
			}catch (SocketTimeoutException ste){

				//ste.printStackTrace();

				// A SocketTimeoutExc. is a simple read timeout, just ignore it.
				// other IOExceptions will not be stopped here.

				if ((heartbeatTimeoutInterval > 0) &&
						((System.currentTimeMillis() - lastRead) > heartbeatTimeoutInterval)){
					try {
						// no reply to heartbeat received.
						// perform a reconnect.
						clientSocket.close();
						clientSocket = new Socket();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}catch (IOException e) {
				// just try to reconnect
				e.printStackTrace();

			}catch (Exception e) {
				Utils.DEBUG(DEBUG_MODE, "Unnexpected error. Exception type: " + e.getClass() + ". Restart connection.");
				e.printStackTrace();
				clientSocket = null;

				// remove listener from serial
				//moteIfFramework.deregisterListener(new WarmSerialMsg(), this);
				//e.printStackTrace();
				//FrameworkSerialThread newThread = new FrameworkSerialThread(this.serialFramework, this.gatewayId, this.dashboardUrl, this.dashboardAddr, this.serverPort, DEBUG_MODE);
				//newThread.run();
			}
		}

	}

	/* Validate JSON Strings */	
	private boolean isJSONValid(String jsonInString) {
		Gson gson = new Gson();

		try {
			Object o = gson.fromJson(jsonInString, Object.class);
		    new GsonBuilder().setPrettyPrinting().create().toJson(o);
			return true;
		} catch(Exception ex) { 
			return false;
		}
	}


	private boolean isJSONStringObject(String jsonString) {

		Gson gson = new Gson();
		try {
			gson.fromJson(jsonString, Map.class);
	    } catch (Exception jsonEx) {
	        return false;
	    }
		
		return true;
	}
	
	/*
	 * this method will start a "heartbeat" message to keep the connection with the server
	 */
	private void registerHeartbeat() {
		ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
		exec.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				if(clientSocket.isConnected()){
					DataOutputStream outToServer;
					try {
						outToServer = new DataOutputStream(clientSocket.getOutputStream());
						outToServer.writeBytes(MSG_HEARTBEAT);
						outToServer.flush();
						Utils.DEBUG(DEBUG_MODE, "Sent heartbeat");
					} catch (IOException e) {
						// keep the thread running
						e.printStackTrace();

						clientSocket = new Socket();
					}
				}
			}
		}, heartbeatInterval / 2, heartbeatInterval, TimeUnit.MILLISECONDS);
	}


	private void decodeJsonPacket(JSONObject json) throws IOException, InterruptedException {
		String verb = (String)json.get("verb");

		if(verb != null) {
			if(verb.equals("created")) {
				Utils.DEBUG(DEBUG_MODE, "Received a new recommendation message");
				JSONObject data = (JSONObject)json.get("data");
				if(data != null) {
					Utils.DEBUG(DEBUG_MODE, "Message: " + json.toJSONString());
					// the node id is reported as long
					Long nodeId = ((Long)data.get("sensorId"));
					Long gwId = ((Long)data.get("gatewayId"));

					if(gwId == this.gatewayId){

						// period is reported as String
						int period = Integer.parseInt((String)data.get("content"));

						Utils.DEBUG(DEBUG_MODE, "The recommendation is to set the node "+ nodeId.intValue() + " to sample every "+period+" seconds");

						if(data.get("startMessage") == null){
							// override other cancellations to the same node
							// this cancellation is verified, it can be sent
							ConfirmedCancelation cancelation = new ConfirmedCancelation(nodeId.intValue(), true);
							queueToCancel.put(nodeId.intValue(), cancelation);
						}

						// override other recommendations to the same node
						// this recommendation is already expired, it can be sent 
						if(period < 0){
							Utils.DEBUG(DEBUG_MODE, "Negative period -- will NOT schedule measurement task in node "+nodeId);
						}else{
							TimestampedRecommendation recommendation = new TimestampedRecommendation(nodeId.intValue(), period * 1000, (new Date()).getTime());
							newRecommendations.put(nodeId.intValue(), recommendation);
						}
					}else{
						Utils.DEBUG(DEBUG_MODE, "Ignoring recommendation. The node is not registered in this network.");
					}


				}
			}
		}
	}




	/**
	 * setup the task in the sink (gateway) node
	 * 
	 * @param nodeId
	 * @throws IOException
	 */
	public void setPrintTaskScheduling(int nodeId) throws IOException {

		Utils.DEBUG(DEBUG_MODE, "Scheduling print task in node "+nodeId);

		byte zero = 0;

		InstantaneousTaskSchedulingRequest printTask = new InstantaneousTaskSchedulingRequest();
		printTask.set_pid(INSTANTANEOUS_TASK_SCHEDULING_REQUEST_PID);
		printTask.set_tid((byte)2);
		printTask.set_ipn((byte)1);
		printTask.set_ta(zero);
		printTask.set_odt(zero);
		printTask.set_odl(zero);
		printTask.set_p(zero);
		printTask.set_ptw(zero);
		printTask.set_ofi(zero);

		int[] ifi_input = new int[16];
		ifi_input[0] = (int) gatewayId;
		printTask.set_input_ifi(ifi_input);

		short[] ifi_pn = new short[16];
		ifi_pn[0] = (short)1;
		printTask.set_input_pn(ifi_pn);

		short[] args = new short[32];
		args[0] = 'T'; 
		args[1] = 'e';
		args[2] = 'm'; 
		args[3] = 'p';
		args[4] = ':'; 
		args[5] = '\0';

		printTask.set_args(args);

		WarmSerialMsg warmMsg = Utils.encapsulateTaskPacket(DEBUG_MODE, printTask, nodeId);
		moteIfFramework.send(nodeId, warmMsg);	
	}


	private void confirmNodeAssociation(int flowId, int nodeId) throws IOException {

		Utils.DEBUG(DEBUG_MODE, "Confirming node association to node "+nodeId);
		NodeAssociateConfirmation confirmation = new NodeAssociateConfirmation();

		confirmation.set_pid(NODE_ASSOCIATE_CONFIRMATION_PID);
		confirmation.set_cfi(flowId);

		WarmSerialMsg warmMsg = Utils.encapsulateTaskPacket(DEBUG_MODE, confirmation, nodeId);
		moteIfFramework.send(nodeId, warmMsg);
	}

	public void requestTaskDescription(int nodeId) throws IOException {

		Utils.DEBUG(DEBUG_MODE, "Requesting task description to node "+nodeId);
		TaskDescriptionRequest request = new TaskDescriptionRequest();

		request.set_pid(NODE_ASSOCIATE_CONFIRMATION_PID);
		request.set_tid(nodeId);

		WarmSerialMsg warmMsg = Utils.encapsulateTaskPacket(DEBUG_MODE, request, nodeId);
		moteIfFramework.send(nodeId, warmMsg);
	}



	/**
	 * handle the message received in the serial communication
	 */
	@Override
	public void messageReceived(int id, Message msg) throws NumberFormatException {

		WarmSerialMsg warmMessage = new WarmSerialMsg(msg.dataGet());
		Utils.DEBUG(DEBUG_MODE, warmMessage.toString());


		int PID = Utils.readPID(warmMessage);

		Utils.DEBUG(DEBUG_MODE, "First byte of data is '"+ PID + "'");

		// origin node
		int nodeId = warmMessage.get_nodeId();

		// check the PID = first byte
		switch(PID){
		// check if it is a JOIN message 
		// If yes, register the node in the Dashboard
		case NODE_CHARACTERISTICS_DESCRIPTION_PID:

			//NodeCharacteristicsDescription nodeDesc = new NodeCharacteristicsDescription(bytesPayload);
			Utils.DEBUG(DEBUG_MODE, "Received characteristics description from node " + nodeId);

			// if we added this node more than 2 JOINs ago, it is a new request
			Date currentTime = new Date();
			Timestamp ttlTimestamp = new Timestamp(currentTime.getTime() - (_PERIODIC_JOIN_TIMEOUT * 2));

			// add new nodes
			if((!registeredNodes.containsKey(nodeId)) || 
					(registeredNodes.get(nodeId).before(ttlTimestamp))){
				registeredNodes.put(nodeId, new Timestamp(currentTime.getTime()));
				getNewNodes().add(nodeId);
			}

			if(nodeId != gatewayId){
				try {
					// TODO use latitude and longitude from nodeDesc
					Utils.addNodeToDashboard(dashboardUrl, gatewayId, nodeId);
					confirmNodeAssociation(warmMessage.get_flowId(), nodeId);
				} catch (Exception e) {
					// print error, if necessary
					e.printStackTrace();
				}

			}else{
				try {
					// TODO use latitude and longitude from nodeDesc
					Utils.addGatewayToDashboard(dashboardUrl, gatewayId);
					confirmNodeAssociation(warmMessage.get_flowId(), nodeId);
				} catch (Exception e) {
					// print error, if necessary
					e.printStackTrace();
				}
			}

			break;

		case TASK_DESCRIPTION_RESPONSE_PID:
			break;

			//case PERIODIC_TASK_SCHEDULING_REQUEST_PID:
			//case INSTANTANEOUS_TASK_SCHEDULING_REQUEST_PID:
		case SCHEDULED_TASK_CANCELLATION_RESPONSE_PID:

			/* remove other confirmations to the same node */
			queueToCancel.remove(nodeId);

			TimestampedRecommendation recommendation = newRecommendations.remove(nodeId);
			/* remove other recommendations to the same node */
			if(recommendation != null){
				// add this recommendation to the queue
				queueRecommendations.put(nodeId, recommendation);
			}
			break;
		case PERIODIC_TASK_SCHEDULING_CONFIRMATION_PID:
		case INSTANTANEOUS_TASK_SCHEDULING_CONFIRMATION_PID:
			// do not try to schedule anymore if the sensor has confirmed
			// remove other recommendation(s) to the same node
			queueRecommendations.remove(nodeId);
			break;
		case NODE_STATISTICS_RESPONSE_PID:

			// TODO: check number of tasks in warmMessage.get_data()

			// get the first in the list that has already been confirmed
			ConfirmedCancelation cancelation = queueToCancel.get(nodeId);
			if(cancelation != null){
				cancelation.setConfirmed(true);
			}
		default:
			break;
		}
	}

}

