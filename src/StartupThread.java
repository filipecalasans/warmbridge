package src;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import src.lib.TimestampedRecommendation;
import src.lib.Utils;

public class StartupThread extends TimerTask {

	private final boolean DEBUG_MODE;
	
	private int gatewayId;
	private int defaultPeriod;
	private FrameworkSerialThread serialThread;
	
	StartupThread (FrameworkSerialThread serialThread, int gatewayId, int defaultPeriod, boolean debugMode){
		this.serialThread = serialThread;
		this.gatewayId = gatewayId;
		this.defaultPeriod = defaultPeriod;
		this.DEBUG_MODE = debugMode;
	}
	
	@Override
	public void run() {
		Utils.DEBUG(DEBUG_MODE, "The period to JOIN is over.");
		
		List<Integer> cloneNewNodes = new ArrayList<Integer>(serialThread.getNewNodes());
		
		for(int nodeId : cloneNewNodes){
			if(nodeId != gatewayId){
				try{
					// start the task with a default value
					TimestampedRecommendation recommendation = new TimestampedRecommendation(nodeId, defaultPeriod, (new Date()).getTime());
							
					// remove other recommendation(s) to the same node
					serialThread.getQueueRecommendations().remove(nodeId);

					// add this recommendation to the queue
					serialThread.getQueueRecommendations().put(nodeId, recommendation);
					
				} catch (Exception e) {
					// print error, if necessary
					e.printStackTrace();
				}
			}else{
				try{
					// tell the gateway that it will receive the measurements
					serialThread.setPrintTaskScheduling(nodeId);
				} catch (Exception e) {
					// print error, if necessary
					e.printStackTrace();
				}
			}
				
		}
		serialThread.getNewNodes().removeAll(cloneNewNodes);
	}

}
