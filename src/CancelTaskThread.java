package src;

import java.util.Random;
import java.util.TimerTask;

import src.lib.ConfirmedCancelation;
import src.lib.Utils;

public class CancelTaskThread extends TimerTask {

	private final boolean DEBUG_MODE;

	private FrameworkSerialThread serialThread; 

	CancelTaskThread (FrameworkSerialThread serialThread, boolean debugMode){
		this.serialThread = serialThread;
		this.DEBUG_MODE = debugMode;
	}

	@Override
	public void run() {
		// Utils.DEBUG(DEBUG_MODE, "Checking cancel queue.");

		if(serialThread.getQueueToCancel().isEmpty()){
			return;
		}

		ConfirmedCancelation cancelation = null;
		
		// get the first in the list that has already been confirmed
		Object [] arrKeys =   serialThread.getQueueToCancel().keySet().toArray();
		this.shuffleArray(arrKeys);
		for(int i = 0; i < arrKeys.length; i++){
			cancelation = serialThread.getQueueToCancel().get((Integer)arrKeys[i]); 
			if(cancelation.isConfirmed()){
				cancelation.setConfirmed(false);
				Utils.cancelTask(DEBUG_MODE, serialThread, cancelation.getSensorId());
				return;
			}
		}

		// otherwise, request a confirmation
		if(cancelation != null){
			Utils.requestStatistics(DEBUG_MODE, serialThread, cancelation.getSensorId());
		}
	}

	// Implementing Fisher–Yates shuffle
	  public void shuffleArray(Object[] ar)
	  {
	    // If running on Java 6 or older, use `new Random()` on RHS here
	    Random rnd = new Random();
	    for (int i = ar.length - 1; i > 0; i--)
	    {
	      int index = rnd.nextInt(i + 1);
	      // Simple swap
	      Object a = ar[index];
	      ar[index] = ar[i];
	      ar[i] = a;
	    }
	  }

}
