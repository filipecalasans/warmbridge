package src.lib;

public class TimestampedRecommendation {
	private final int sensorId;
	private final int recommendation;
	private long ttl;

	public TimestampedRecommendation(int sensor, int recommendation_val, long ttl_val){
		this.sensorId = sensor;
		this.recommendation = recommendation_val;
		this.ttl = ttl_val;
	}

	public int getSensorId() {
		return sensorId;
	}
	
	public int getRecommendation() {
		return recommendation;
	}
	public long getTTL() {
		return ttl;
	}
	
	public void setTTL(long ttl_val) {
		this.ttl = ttl_val;
	}

}
