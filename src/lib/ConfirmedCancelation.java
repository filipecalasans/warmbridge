package src.lib;

public class ConfirmedCancelation {
	private final int sensorId;
	private boolean confirmed;
	
	public ConfirmedCancelation(int sensor, boolean confirmed){
		this.sensorId = sensor;
		this.confirmed = confirmed;
	}

	public int getSensorId() {
		return sensorId;
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

}
