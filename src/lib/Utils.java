package src.lib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javapackets.NodeStatisticsRequest;
import javapackets.PeriodicTaskSchedulingRequest;
import javapackets.ScheduledTaskCancellationRequest;
import javapackets.WarmSerialMsg;
import src.FrameworkSerialThread;

public class Utils {

	private  static final byte PERIODIC_TASK_SCHEDULING_REQUEST_PID = 0x3;
	private static final byte SCHEDULED_TASK_CANCELLATION_REQUEST_PID = 0x6;
	private static final byte NODE_STATISTICS_REQUEST_PID = 0x8;
	
	private static final String USER_AGENT = "Mozilla/5.0";

	private static final Logger log = Logger.getLogger(Utils.class.getName());
	
	public static void addGatewayToDashboard(String dashboardUrl, int gatewayId) throws Exception {

		String urlParameters =  "?lat=-23.556996&lon=-46.730387&idgw=" + gatewayId;

		String url = dashboardUrl + "/newGateway" + urlParameters ; 

		sendGET(url);
	}

	public static void addNodeToDashboard(String dashboardUrl, int gatewayId, int nodeId) throws Exception {

		String urlParameters =  "?lat=-23.556996&lon=-46.730387&id=" + nodeId + "&idgw=" + gatewayId;

		String url = dashboardUrl + "/newSensor" + urlParameters ; 

		sendGET(url);
	}


	public static void sendMeasurementToDashboard(String dashboardUrl, int nodeId, double measurement, int count) throws Exception {
		// format compact temperature
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
		otherSymbols.setDecimalSeparator('.'); // force "point" as decimal separator
		otherSymbols.setGroupingSeparator(','); 
		DecimalFormat df = new DecimalFormat("#.00", otherSymbols);

		String urlParameters =  "?sensor=" + nodeId + "&" + 
				"temp=" + df.format(measurement) + "&" + 
				"counter=" + count;

		String url = dashboardUrl + "/newMeasurement" + urlParameters ; 

		sendGET(url);
	}

	/**
	 * send a GET request to the url
	 * 
	 * @param url
	 * @throws Exception
	 */
	private static void sendGET(String url) throws Exception {


		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", Utils.USER_AGENT);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
	}

	/** 
	 * read the PID from the data
	 * @param warmMessage
	 * @return
	 */
	public static int readPID(WarmSerialMsg warmMessage) {
		ByteBuffer byteBuf = ByteBuffer.allocate(2);
		byteBuf.order(ByteOrder.LITTLE_ENDIAN);
		byteBuf.asShortBuffer().put(warmMessage.dataGet()[WarmSerialMsg.offset_data(0)]);
		byte [] bytesPayload  = byteBuf.array();
		
		return ((bytesPayload[0] & 15) << 4) | ((bytesPayload[0] >> 4) & 15);
	}

	public static void DEBUG(boolean isDebug, String msg){
		if(isDebug){
			System.out.println(msg);
		}
	}

	/**
	 * encapsulateTaskPacket - prepare the message to send via serial
	 * 
	 * @param message
	 * @param nodeId
	 * @return
	 */
	public static WarmSerialMsg encapsulateTaskPacket(boolean debug_mode, final net.tinyos.message.Message message, final int nodeId) {
		WarmSerialMsg warmMsg = new WarmSerialMsg();
		warmMsg.set_flowId(nodeId);
		warmMsg.set_nodeId(nodeId);
		warmMsg.set_len((short)message.dataLength());

		short[] data = new short[message.dataLength()];
		for(int i=0; i<message.dataLength(); i++) {
			data[i] = message.dataGet()[i];
		}

		Utils.DEBUG(debug_mode, "to: " + warmMsg.get_nodeId() + "\nflow id: " + warmMsg.get_flowId());
		Utils.DEBUG(debug_mode, "data length: " + data.length);

		System.out.print("payload: [ ");
		for(int i=0; i<data.length; i++) {
			System.out.print(String.format("%d ", data[i]));
		}

		Utils.DEBUG(debug_mode, "]");
		warmMsg.set_data(data);
		return warmMsg;
	}


	/**
	 * setup the task to measure temperature 
	 * 
	 * @param nodeId
	 * @param taskPeriod
	 * @throws IOException
	 */
	public static void setRecommendation(boolean debug_mode, FrameworkSerialThread serialThread, int nodeId, int gatewayId, int periodInMilliseconds){

		if(periodInMilliseconds < 0){
			Utils.DEBUG(debug_mode, "Negative period -- will NOT schedule measurement task in node "+nodeId);
			return;
		}

		Utils.DEBUG(debug_mode, "Scheduling measurement task in node "+nodeId);

		PeriodicTaskSchedulingRequest periodicTask = new PeriodicTaskSchedulingRequest();

		periodicTask.set_pid(PERIODIC_TASK_SCHEDULING_REQUEST_PID);
		periodicTask.set_tid((byte)1);
		periodicTask.set_x((byte)0x0);
		periodicTask.set_ta((byte)0x0);
		periodicTask.set_odt((byte)5);
		periodicTask.set_odl((byte)2);
		periodicTask.set_p((byte)0);
		periodicTask.set_tp(periodInMilliseconds);
		periodicTask.set_ofi(gatewayId);

		WarmSerialMsg warmMsg = Utils.encapsulateTaskPacket(debug_mode, periodicTask, nodeId);
		try {
			log.log(Level.INFO, "setting recommendation (node "+nodeId+")");
			serialThread.getMoteIfFramework().send(nodeId, warmMsg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void cancelTask(boolean debug_mode, FrameworkSerialThread serialThread, int nodeId) {
		Utils.DEBUG(debug_mode, "Cancelling measurement task in node "+nodeId);

		ScheduledTaskCancellationRequest cancelTask = new ScheduledTaskCancellationRequest();

		cancelTask.set_pid(SCHEDULED_TASK_CANCELLATION_REQUEST_PID);
		cancelTask.set_tid((byte)1);
		cancelTask.set_tin((byte)0);

		WarmSerialMsg warmMsg = Utils.encapsulateTaskPacket(debug_mode, cancelTask, nodeId);
		try {
			log.log(Level.INFO, "cancelling measurement (node "+nodeId+")");
			serialThread.getMoteIfFramework().send(nodeId, warmMsg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void requestStatistics(boolean debug_mode, FrameworkSerialThread serialThread, int nodeId) {
		Utils.DEBUG(debug_mode, "Cancelling measurement task in node "+nodeId);

		NodeStatisticsRequest request = new NodeStatisticsRequest();

		request.set_pid(NODE_STATISTICS_REQUEST_PID);

		WarmSerialMsg warmMsg = Utils.encapsulateTaskPacket(debug_mode, request, nodeId);
		try {
			log.log(Level.INFO, "requesting statistics (node "+nodeId+")");
			serialThread.getMoteIfFramework().send(nodeId, warmMsg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
