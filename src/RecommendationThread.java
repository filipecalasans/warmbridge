package src;

import java.util.Date;
import java.util.TimerTask;

import src.lib.TimestampedRecommendation;
import src.lib.Utils;

public class RecommendationThread extends TimerTask {

	private final boolean DEBUG_MODE;


	private int gatewayId;
	private FrameworkSerialThread serialThread;

	RecommendationThread (FrameworkSerialThread serialThread, int gatewayId, boolean debugMode){
		this.serialThread = serialThread;
		this.gatewayId = gatewayId;
		this.DEBUG_MODE = debugMode;
	}


	@Override
	public void run() {
		// Utils.DEBUG(DEBUG_MODE, "Checking cancel queue.");

		if(serialThread.getQueueRecommendations().isEmpty()){
			return;
		}

		// get one that has been already canceled and is expired
		Object [] arrKeys = serialThread.getQueueRecommendations().keySet().toArray();
		for(int i = 0; i < arrKeys.length; i++){
			TimestampedRecommendation recommendation =  serialThread.getQueueRecommendations().get((Integer)arrKeys[i]);
			// check if this recommendation can be sent, i.e., it has expired
			if(recommendation.getTTL() < (new Date()).getTime()){
				
				if(!serialThread.getQueueToCancel().containsKey(recommendation.getSensorId())){
					// this has already been canceled, a new recommendation can be sent
					Utils.setRecommendation(DEBUG_MODE, serialThread, recommendation.getSensorId(), gatewayId, recommendation.getRecommendation());
					
					// set new expiration: the time to deliver this packet + small backoff (1000ms)
					recommendation.setTTL((new Date()).getTime() + (2*recommendation.getRecommendation()) + 1500);
	
					return;
				}
			}
		}
		
	}

}
