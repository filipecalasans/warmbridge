package src;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.tinyos.message.Message;
import net.tinyos.message.MessageListener;
import net.tinyos.message.MoteIF;
import net.tinyos.message.SerialPacket;
import net.tinyos.packet.BuildSource;
import net.tinyos.packet.PhoenixSource;
import net.tinyos.util.PrintStreamMessenger;
import src.lib.Utils;



public class SinkSerialThread  implements Runnable, MessageListener{


	PhoenixSource phoenix;
	MoteIF moteIf;
	LinkedList<Socket> clientSockets;

	final public static int SERVER_PORT = 60000;
	ServerSocket serverSocket;
	String serialSource;

	private final String dashboardUrl;
	private final boolean DEBUG_MODE;


	int count = 0;

	public SinkSerialThread(String serialSource, String dasboardAddr, boolean debugMode) throws Exception  {
		this.dashboardUrl = dasboardAddr;
		this.DEBUG_MODE = debugMode;

		clientSockets = new LinkedList<Socket>();
		connectSerial(serialSource);

		serverSocket = new ServerSocket(SERVER_PORT);
		this.serialSource = serialSource;

	}

	private void connectSerial(String serialSource) {
		phoenix = BuildSource.makePhoenix(serialSource, PrintStreamMessenger.err);
		moteIf = new MoteIF(phoenix);
		SerialPacket msg = new SerialPacket();
		msg.amTypeSet(100);
		moteIf.registerListener(msg, this);
	}

	public void run(){


		while(true){
			// check every second if the connection is still alive
			if(phoenix.isAlive()){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}	
			}else{
				// otherwise, finish the thread
				Utils.DEBUG(DEBUG_MODE, "Phoenix connection is closed in the Gateway. ");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Utils.DEBUG(DEBUG_MODE, "Trying to create a new Phoenix connection. ");
				connectSerial(this.serialSource);
				break;
			}
		}
	}


	@Override
	public void messageReceived(int id, Message msg) {

		SerialPacket packet = (SerialPacket)msg;
		Utils.DEBUG(DEBUG_MODE, packet.toString());


		/*  String Format
		 *  
		 *  printf("\n%s %d (Origin: node %d)",
		 *  argString[taskInstance],
		 *  dataToPrint,
		 *  dataOriginId);
		 * 
		 *  Expected string: Temp: %d (Origin: node %d)
		 **/

		String receivedString = new String(packet.dataGet());
		Utils.DEBUG(DEBUG_MODE, "Received message: " + receivedString.trim());
		Pattern patt = Pattern.compile("\\s*[\\w:]+\\s*(\\d+)\\s*\\(Origin:\\s*(node\\s*)?(\\d+)\\)");
		Matcher match = patt.matcher(receivedString.trim());

		if (match.matches()) {
			try {
				int temperature = Integer.parseInt(match.group(1));
				// this is for internal temperature
				//double tempConverted = ((((double)temperature / 4096) * 1.5) - 0.986) / 0.00355;
				// this is for Sensirion SH11 sensor
				double tempConverted = ((double)temperature * 0.01) - 40 ;
				int nodeId  = Integer.parseInt(match.group(3));

				Utils.DEBUG(DEBUG_MODE, "<"+ temperature +" (" + tempConverted +"), "+ nodeId +">");

				Utils.sendMeasurementToDashboard(dashboardUrl, nodeId, tempConverted, count++);
			} catch (Exception e) {
				// print error, but do not react
				e.printStackTrace();
			}
		}else{
			Utils.DEBUG(DEBUG_MODE, "Does not contain a temperature.");
		}
	}
}