#!/bin/bash

argument=$1

if [[ $argument =~ (.*)Temp:\ (.*)Origin:\ *([0-9]+)\) ]]; then
	# received a temp message
	id_sensor="${BASH_REMATCH[3]}"
	echo "${id_sensor},measurement,$(date +'%s')" >> ../logs/log.csv
elif [[ $argument =~ (.*)cancelling\ measurement\ \(node\ ([0-9]+)\) ]]; then
	# sent a cancel message
	id_sensor="${BASH_REMATCH[1]}"
	echo "${id_sensor},cancel,$(date +'%s')" >> ../logs/log.csv
elif [[ $argument =~ (.*)setting\ recommendation\ \(node\ ([0-9]+)\) ]]; then
	#sent a schedule message
	id_sensor="${BASH_REMATCH[1]}"
	echo "${id_sensor},schedule,$(date +'%s')" >> ../logs/log.csv
elif [[ $argument =~ (.*)requesting\ statistics\ \(node\ ([0-9]+)\) ]]; then
	# retransmitted -- requested statistics
	id_sensor="${BASH_REMATCH[1]}"
	echo "${id_sensor},retransmission,$(date +'%s')" >> ../logs/log.csv
fi
