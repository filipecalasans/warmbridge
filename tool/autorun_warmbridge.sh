### BEGIN INIT INFO
# Provides:          warmbridge
# Required-Start:    $remote_fs $syslog $networking
# Required-Stop:     $remote_fs $syslog $networking
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Start WARMBRIDGE as a daemon.
### END INIT INFO
#!/bin/bash

FOLDER_WARMBRIDGE="/home/gabriel/Downloads/warmbridge"

# set serial identifications
#MOTE_THREE="XBQYN747"
#MOTE_FOUR="XBQYN1HD"
MOTE_GATEWAY_CODE=${1}
MOTE_FRAMEWORK_CODE=${2}
MOTE_GATEWAY_ID=${3}

# get id from motelist
USB_MOTE_THREE=`motelist | grep -Po "${MOTE_FRAMEWORK_CODE}\s*/dev/ttyUSB\K[^ ]"`
USB_MOTE_FOUR=`motelist | grep -Po "${MOTE_GATEWAY_CODE}\s*/dev/ttyUSB\K[^ ]"`

# update warm from GIT
cd ${FOLDER_WARMBRIDGE}
git pull

# start warmbridge
if [ ! -z ${USB_MOTE_THREE} ]; then
	if [ ! -z ${USB_MOTE_FOUR} ]; then
		java -jar ${FOLDER_WARMBRIDGE}/bin/warmbridge.jar \
		 -dashboard 104.155.131.234 -framework "serial@/dev/ttyUSB${USB_MOTE_THREE}:115200" \
		 -gwid ${MOTE_GATEWAY_ID} -port 7600 -webport 1337 -sink "serial@/dev/ttyUSB${USB_MOTE_FOUR}:115200" -debug & # -debug > "${FOLDER_WARMBRIDGE}/log.txt"  2> "${FOLDER_WARMBRIDGE}/errors.txt" & 

	fi
else
	echo "did not find usb" >> "${FOLDER_WARMBRIDGE}/log.txt"
fi

exit 0

